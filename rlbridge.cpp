#include "lv2.h"
#include "client.hpp"

#include <cstdlib>


#define INPUT_1 0
#define INPUT_2 1
#define INPUT_3 2
#define INPUT_4 3
#define INPUT_5 4
#define INPUT_6 5
#define INPUT_7 6
#define INPUT_8 7
#define OUTPUT_1 8
#define OUTPUT_2 9
#define OUTPUT_3 10
#define OUTPUT_4 11
#define OUTPUT_5 12
#define OUTPUT_6 13
#define OUTPUT_7 14
#define OUTPUT_8 15

BridgeClient *bridge_client;

extern "C"
{

    static LV2_Descriptor *RLBridgeDescriptor = NULL;

    typedef struct
    {
        float* inputs[8];
        float* outputs[8];
    } RLBridge;

    static void cleanupRLBridge(LV2_Handle instance)
    {
        free(instance);
    }


    static void connectPortRLBridge(LV2_Handle instance, uint32_t port, void *data)
    {
        RLBridge *plugin = (RLBridge *)instance;

        switch (port)
        {
        case INPUT_1:
            plugin->inputs[0] = (float*)data;
            break;
        case INPUT_2:
            plugin->inputs[1] = (float*)data;
            break;
        case INPUT_3:
            plugin->inputs[2] = (float*)data;
            break;
        case INPUT_4:
            plugin->inputs[3] = (float*)data;
            break;
        case INPUT_5:
            plugin->inputs[4] = (float*)data;
            break;
        case INPUT_6:
            plugin->inputs[5] = (float*)data;
            break;
        case INPUT_7:
            plugin->inputs[6] = (float*)data;
            break;
        case INPUT_8:
            plugin->inputs[7] = (float*)data;
            break;
        case OUTPUT_1:
            plugin->outputs[0] = (float*)data;
            break;
        case OUTPUT_2:
            plugin->outputs[1] = (float*)data;
            break;
        case OUTPUT_3:
            plugin->outputs[2] = (float*)data;
            break;
        case OUTPUT_4:
            plugin->outputs[3] = (float*)data;
            break;
        case OUTPUT_5:
            plugin->outputs[4] = (float*)data;
            break;
        case OUTPUT_6:
            plugin->outputs[5] = (float*)data;
            break;
        case OUTPUT_7:
            plugin->outputs[6] = (float*)data;
            break;
        case OUTPUT_8:
            plugin->outputs[7] = (float*)data;
            break;
        }
    }

    static LV2_Handle instantiateRLBridge (const LV2_Descriptor * descriptor, double s_rate, const char * bundle_path,
                                           const LV2_Feature * const * host_features)
    {
        RLBridge *plugin_data = (RLBridge *)malloc(sizeof(RLBridge));

        bridge_client = new BridgeClient();

        return (LV2_Handle)plugin_data;
    }

    static void runRLBridge(LV2_Handle instance, uint32_t sample_count)
    {
        RLBridge *plugin_data = (RLBridge *)instance;

        float input[sample_count * 8];
		float output[sample_count * 8];

		for (unsigned int i = 0; i < sample_count; i++)
        {
			for (int c = 0; c < 8; c++)
            {
				input[8 * i + c] = plugin_data->inputs[c][i];
			}
		}

        bridge_client->processStream(input, output, sample_count);

        for (unsigned int o = 0; o < sample_count; o++)
        {
			for (int c = 0; c < 8; c++)
            {
				plugin_data->outputs[c][o] = output[8 * o + c];
			}
		}
    }

    static void init()
    {
        RLBridgeDescriptor = (LV2_Descriptor *)malloc(sizeof(LV2_Descriptor));

        RLBridgeDescriptor->URI = "http://rlbridge.sonusdept.com";
        RLBridgeDescriptor->activate = NULL;
        RLBridgeDescriptor->cleanup = cleanupRLBridge;
        RLBridgeDescriptor->connect_port = connectPortRLBridge;
        RLBridgeDescriptor->deactivate = NULL;
        RLBridgeDescriptor->instantiate = instantiateRLBridge;
        RLBridgeDescriptor->run = runRLBridge;
        RLBridgeDescriptor->extension_data = NULL;
    }

    LV2_SYMBOL_EXPORT
    const LV2_Descriptor *lv2_descriptor(uint32_t index)
    {
        if (!RLBridgeDescriptor) init();

        switch (index)
        {
        case 0:
            return RLBridgeDescriptor;
        default:
            return NULL;
        }
    }

}