This is a simple LV2 plugin that acts as bridge between VCV Rack and your favourite LV2 host.

To build the plugin, clone this repository inside Rack main folder (or anywhere else, if you manually set the variable RACK_DIR), and run make.

To install it, copy the resulting rlbridge.lv2 folder inside your LV2 plugins folder (often /usr/lib/lv2/ or ~/.lv2/).
