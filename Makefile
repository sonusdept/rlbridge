PLUGIN_NAME = rlbridge

VERSION = 0.6.0

RACK_DIR ?= ..
include $(RACK_DIR)/arch.mk

BUNDLE_FILES = manifest.ttl $(PLUGIN_NAME).ttl $(PLUGIN_NAME).so

CXXFLAGS = -fPIC -Wall -O2 -DPIC -fomit-frame-pointer

GENDEP_SED_EXPR = "s/^\\(.*\\)\\.o *: /$(subst /,\/,$(@:.d=.o)) $(subst /,\/,$@) : /g"
GENDEP_C = set -e; $(GCC_PREFIX)g++ -MM $(CXXFLAGS) $< | sed $(GENDEP_SED_EXPR) > $@; [ -s $@ ] || rm -f $@


all:
	@g++ $(CXXFLAGS) $(FLAGS) -lpthread -shared -fPIC -I$(RACK_DIR)/include $(LDFLAGS) *.cpp -o $(PLUGIN_NAME).so
	@echo "Creating LV2 bundle $@ ..."
	@rm -rf $(PLUGIN_NAME).lv2
	@mkdir $(PLUGIN_NAME).lv2
	@cp $(BUNDLE_FILES) $(PLUGIN_NAME).lv2

clean:
	rm -rf *.o $(PLUGIN_NAME).so $(PLUGIN_NAME).lv2
